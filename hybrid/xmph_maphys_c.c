#include "xmph_maphys_type_c.h"
#include "mpi.h"

extern void XMPH_maphys_driver_c2f(int job, MPI_Fint comm, int sym, int n, int nnz,
                                           int *rows, int *cols, XMPH_CFLOAT *values, int nrhs,
                                           XMPH_CFLOAT *rhs, XMPH_CFLOAT *sol, int *icntl, double *rcntl,
                                           int *iinfo, double *rinfo, int *iinfog, double *rinfog,
                                           int *iinfomin, int *iinfomax, int *iinfoavg,
                                           int *iinfosig, double *rrinfomin, double *rrinfomax,
                                           double *rrinfoavg, double *rinfosig);
			       
extern void XMPH_maphys_create_domain_c2f( int myndof, int myndofinterior, int myndofintrf, 
					   int mysizeintrf, int lenindintrf,  int gballintrf, int mynbvi, int* myindexvi,
					   int * myptrindexvi, int* myindexintrf, int* myinterface, int myndoflogicintrf, int *mylogicintrf,
					   double* weight);

void xmph_maphys_driver_c(XMPH_maphys_t_c* maphys_par)
{
  /*
   * The following local variables will 
   *  be passed to the F77 interface.
   */  
  /* Call Fortran interface interface */
  
  (maphys_par->fcomm) = MPI_Comm_c2f((maphys_par->comm));

  if (maphys_par->job == -1) {

    maphys_par->rows = NULL;
    maphys_par->cols = NULL;
    maphys_par->values = NULL;
    maphys_par->rhs = NULL;
    maphys_par->sol = NULL;
  }
  XMPH_maphys_driver_c2f((maphys_par->job), (maphys_par->fcomm), (maphys_par->sym), (maphys_par->n), (maphys_par->nnz),
			 (maphys_par->rows), (maphys_par->cols), (maphys_par->values), maphys_par->nrhs,
			 (maphys_par->rhs), maphys_par->sol, (maphys_par->icntl), (maphys_par->rcntl),
			 maphys_par->iinfo, (maphys_par->rinfo), (maphys_par->iinfog), (maphys_par->rinfog),
			 (maphys_par->iinfomin), (maphys_par->iinfomax), (maphys_par->iinfoavg),
			 (maphys_par->iinfosig), (maphys_par->rinfomin), (maphys_par->rinfomax),
			 (maphys_par->rinfoavg), (maphys_par->rinfosig));
}



void xmph_maphys_create_domain_c(XMPH_maphys_t_c * maphys_par, int myndof, int myndofinterior, int myndofintrf, 
				 int lenindintrf, int mysizeintrf, int gballintrf, int mynbvi, int* myindexvi,
				 int * myptrindexvi, int* myindexintrf, int* myinterface, int myndoflogicintrf, int *mylogicintrf,
				 double* weight)  {
    

  /* Call Fortran interface */
  



  

  XMPH_maphys_create_domain_c2f( myndof, myndofinterior, myndofintrf, mysizeintrf,
				 lenindintrf, gballintrf, mynbvi, myindexvi,
				 myptrindexvi, myindexintrf, myinterface, myndoflogicintrf,
				 mylogicintrf, weight);  


}


void XMPH_distributed_interface_c(XMPH_maphys_t_c * maphys_par, int myndof,
				  int mysizeintrf, int* myinterface, int mynbvi, int* myindexvi,
				  int * myptrindexvi, int* myindexintrf) {
    

      /* Call Fortran interface */
      XMPH_distributed_interface_c2f( myndof, mysizeintrf,
				      myinterface, mynbvi, myindexvi,
				      myptrindexvi, myindexintrf);

}
