/* @authors P. HENON */

#ifndef PHIDAL_COMMON_H
#define PHIDAL_COMMON_H

#include <assert.h>

#include "hips_define.h"
#include "queue.h"
#include "phidal_struct.h"


/**** For debuging only ****/
void *MonMalloc(size_t size);
void *MonRealloc(void *ptr, size_t size);
void MonFree(void *ptr);



/*** Misc func ***/
extern  COEF my_zdotc(blas_t n, COEF *x, blas_t incx, COEF *y, blas_t incy);

void writeVecInt(FILE *fp, dim_t *vec, dim_t n);
void readVecInt(FILE *fp, dim_t *vec, dim_t n);
void writeVecCoef(FILE *fp, COEF *vec, dim_t n);
void readVecCoef(FILE *fp, COEF *vec, dim_t n);


void ascend_column_reorder(csptr mat);
void GetCommonRows(csptr a1, csptr a2, int *nr, int *nrtab);
int  find_dichotom(int col, int start, int end, dim_t *ja);
void IntersectRow(dim_t n1, dim_t *ja1, dim_t n2, dim_t *ja2, int *n, int *indtab);
void CS_IntersectRow(dim_t n1, dim_t *ja1, csptr *ma1, dim_t n2, dim_t *ja2, csptr *ma2, int *nb, csptr *wkcs1, csptr *wkcs2);
void union_set(int *set1, dim_t n1, int *set2, dim_t n2, int **set, int *n);
void UnionSet(int *set1, dim_t n1, int *set2, dim_t n2, int *set, int *n);
int set_delete(int **list, int *size, int elt);
void set_add(int **list, int *size, int elt);
void IntersectSet(dim_t *ja1, dim_t n1, dim_t *ja2, dim_t n2, dim_t *ja, int *n);
void dumpcsr(FILE *fp, COEF *a, dim_t *ja, INTL *ia, dim_t n);
void dumpCS(int num, FILE *fp, csptr A);


/*** This are the declarations of some internal functions of the PHIDAL library ***/
/* void read_ijv(char *filename, int *n, int *nnz, int **iaptr, int **japtr, COEF **maptr); */
/* void read_ijv3(char *filename, int *n, int *nnz, int **iaptr, int **japtr, COEF **maptr); */
void GENERAL_setpar(char *filename,  char *matrix, char *sfile_path, int *unsym, int *rsa, PhidalOptions *option);
void GRID_setpar(char *filename,  int *nc, int *cube, char *sfile_path, int *unsym, int *rsa,  PhidalOptions *option);
void PhidalOptions_Print(FILE *file, PhidalOptions *option);
void PhidalOptions_Fix(PhidalOptions *options, PhidalHID *BL);

void HID_Info(FILE *file, PhidalHID *BL);

void CSR_Write(FILE *fp, flag_t symmetric, dim_t n, INTL *ia, dim_t *ja, COEF *a);
void CSR_Load(FILE *fp, flag_t *symmetric, dim_t *n, INTL **ia, dim_t **ja, COEF **a);
void CSR_Cnum2Fnum(dim_t *ja, INTL *ia, dim_t n);
void CSR_Fnum2Cnum(dim_t *ja, INTL *ia, dim_t n);
void CSR_ComplementSubmatrix(dim_t nn, dim_t *nodelist, dim_t n, INTL *ia, dim_t *ja, COEF *ma,
			     INTL **sia, dim_t **sja, COEF **sma);
void CSR_GetSquareSubmatrix(dim_t nn, dim_t *nodelist, dim_t n, INTL *ia, dim_t *ja, COEF *ma, INTL **sia, dim_t **sja, COEF **sma);
void CSR_RowPerm(dim_t n, INTL *ia, dim_t *ja, COEF *a, dim_t *perm);
void CSR_ColPerm(dim_t n, INTL *ia, dim_t *ja, COEF *a, dim_t *perm);
void CSR_Perm(dim_t n, INTL *ia, dim_t *ja, COEF *a, dim_t *perm);
void CSR_SuppressDouble(flag_t op, dim_t n, INTL *ia, dim_t *ja, COEF* a);

void CS_SetNonZeroRow(csptr m);
void CS_SymmetrizeTriang(flag_t job, char *UPLO, csptr mat);
void CS_Transpose(flag_t job, csptr mat, int coldim);
int CS_RowPerm(csptr mat, dim_t *perm);
int CS_ColPerm(csptr mat, dim_t *perm) ;
int CS_Perm(csptr mat, dim_t *perm);
void CS_Reorder(csptr m, dim_t *permtab, dim_t *ipermtab);
void CS_DropT(csptr amat, REAL droptol, REAL *droptab);
void CS_DropT_SaveDiag(csptr amat, REAL droptol, REAL *droptab);


int CSR_SuppressZeros(int numflag, dim_t n, INTL *ia, dim_t *ja, COEF *a);
int CSRcs(dim_t n, COEF *a, dim_t *ja, INTL *ia, csptr bmat);
int CSRcs_InaRow(dim_t n, COEF *a, dim_t *ja, INTL *ia, csptr bmat);
void cs2csr(csptr mat, INTL *ia, dim_t *ja ,COEF *a);
long CSnnz(csptr mat);
int CSrealloc(csptr mat);
int CSunrealloc(csptr mat);
void CS_Copy(csptr amat, csptr bmat);
void CS_Move(csptr amat, csptr bmat);
REAL CSnormSquare(csptr mat);
REAL CSnormFrob(csptr mat);
REAL CSnorm1(csptr mat);
REAL norm2(COEF *x, dim_t n);
void CS_MatricesAdd(csptr a, int matnbr, csptr mattab, int *tmpj, COEF *tmpa, dim_t *jrev);
void CS_MatricesGather(csptr a, int matnbr, csptr mattab, int *tmpj, COEF *tmpa, dim_t *jrev);

void matvec(csptr mata, COEF *x, COEF *y);
void matvecz(csptr mata, COEF *x, COEF *y, COEF *z);
void matvec_add(csptr mata, COEF *x, COEF *y);
void NoDiag_matvec_add(csptr mata, COEF *x, COEF *y);
void CSC_matvec_add(csptr mata, COEF *x, COEF *y);
void CSC_matvec_add(csptr mata, COEF *x, COEF *y);
void CSC_NoDiag_matvec_add(csptr mata, COEF *x, COEF *y);
void CSC_matvecz(csptr mata, COEF *x, COEF *y, COEF *z);
void csr_matvec_add(dim_t n, INTL *ia, INTS *ja, COEF *a, COEF *x, COEF *y);


void CSR_Lsol(flag_t unitdiag, csptr mata, COEF *b, COEF *x);
void CSR_Usol(flag_t unitdiag, csptr mata, COEF *b, COEF *x);
void CSC_Lsol(flag_t unitdiag, csptr mata, COEF *b, COEF *x);
void CSC_Usol(flag_t unitdiag, csptr mata, COEF *b, COEF *x);
void CSC_Ltsol(flag_t unitdiag, csptr mata, COEF *b, COEF *x);

void extract_mat(csptr amat, csptr bmat, dim_t nrow, dim_t ncol, int *rowtab, int *coltab);
void phidal_block_pattern(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, csptr P, int_t locally_nbr, PhidalHID *BL);

int block_in_pattern(int i, int j, int_t locally_nbr, PhidalHID *BL, int *tmpkey);
void intersect_key(int nk1, int nk2, int *block_key1, int *block_key2, int *ret_key, int *nk);
int is_intersect_key(int kl1, int *key1, int  kl2, int *key2);
int is_in_key(const int p, const int block, const int *block_keyindex, const int *block_key);
int is_in_key2(const int p, const INTL block, const INTL *block_keyindex, const int *block_key);
int is_equal_key(int *key1, int kl1, int *key2, int kl2);
int key_compare(int kl1, int *key1, int kl2, int *key2);

int compare_node(int node1, int node2, int keysize, int *keyindex, int *key);

void CS_GetLower(flag_t job, char *DIAG, csptr a, csptr l);
void CS_GetUpper(flag_t job, char *DIAG, csptr a, csptr u);
void CSR_GetUpper(flag_t numflag, dim_t n, INTL *ia, dim_t *ja, COEF *a);

void CS_RowMult(COEF *vec, csptr m);
void CS_RowMult2(REAL *vec, csptr m);
void CS_ColMult(COEF *vec, csptr m);
void CS_ColMult2(REAL *vec, csptr m);

void CS_ILUT(csptr A, csptr L, csptr U, REAL droptol, REAL *droptab, REAL fillrat, Heap *heap, int *wki1, int *wki2, COEF *wkd);
void CS_ILUTP(csptr A, csptr L, csptr U,  REAL pivot_ratio, REAL droptol, REAL *droptab, REAL fillrat, dim_t *permtab, Heap *heap, int *wki1, int *wki2, COEF *wkd);
void CSR_CSR_InvLT(csptr L, csptr M, int Mncol, REAL droptol, REAL *droptab, REAL fillrat, int *wki1, int *wki2, COEF *wkd);
void CSR_CSR_InvUT(csptr U, csptr M, REAL droptol, REAL *droptab,  REAL fillrat, Heap *heap, int *wki1, int *wki2, COEF *wkd);
void  CSRrowMultCSRcol(REAL droptol, REAL *droptab, REAL fillrat, int nnb, COEF alpha, csptr *X, csptr *Y, csptr m, int mncol, int *wki1, int *wki2, COEF *wkd);


/*void CS_ILUC(csptr L, csptr U, REAL droptol, REAL *droptab, REAL fillrat, int *wki1, int *wki2, REAL *wkd);*/

void CS_ICCT(csptr L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, int *wki1, int *wki2, COEF *wkd, cell_int *firsttab, cell_int **LrowList, flag_t shiftdiag);
void CSC_CSR_InvLT(csptr  L, csptr M,  int Mncol, REAL droptol, REAL *droptab, REAL fillrat, int *wki1, int *wki2, COEF *wkd, cell_int *celltab, cell_int **LrowList);
void CSCxCSR_CSC_GEMMT(COEF alpha, csptr A, csptr B, csptr C, int Cnrow, REAL droptol, REAL *droptab, REAL fillrat, int *wki1, int *wki2, COEF *wkd, cell_int *celltab, cell_int **BcolList);
void CS_ICCprod(csptr A, csptr L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, int *wki1, int *wki2, COEF *wkd, cell_int *celltab, cell_int **ArowList);

void CSCrowMultCSRcol(REAL droptol, REAL *droptab, REAL fillrat, dim_t nnb, COEF alpha, csptr *X, csptr *Y, csptr m, dim_t mnrow, int *wki1, int *wki2, COEF *wkd, cell_int *celltab, cell_int **BcolList);

void CSCrowICCprod(REAL droptol, REAL *droptab, REAL fillrat, dim_t nnb, csptr *X, csptr l, COEF *D, int *wki1, int *wki2, COEF *wkd, cell_int *celltab, cell_int **BcolList);
void CS_VirtualMerge(int nb, csptr *mattab, csptr amat);
void CS_VirtualSplit(csptr amat, int nnb, csptr *mattab);

void vec_filldrop(dim_t *nnz, dim_t *ja, COEF *ma, dim_t p, Queue *heap);
void LU_filldrop(dim_t *nnz1, dim_t *ja1, COEF *ma1, dim_t *nnz2, dim_t *ja2, COEF *ma2, dim_t p, Queue *heap);

void set_coeff(csptr A, csptr P);
void sort_row(csptr P);
void quicksort_row(dim_t *ja, COEF *a, int start, int end);
void quicksort(int *inttab, int start, int end);
void quicksort64(INTL *inttab, INTL start, INTL end);
void quicksort_int(dim_t *nodelist, int start, int end, int *noderank);
void quicksort_node(dim_t *nodelist, int start, int end, int keysize, int *keyindex, int *key);
void quicksort_node_tags(int *nodelist, int start, int end, int* tags, int tagnbr);
int compare_node_tags(dim_t node1, dim_t node2, int* tags, int tagnbr);

int PhidalMatrix_Check(PhidalMatrix *M, PhidalHID *BL);
void CS_Check(csptr m, int dim2);
void checkGraph(dim_t n, INTL *ia, dim_t *ja, INTL *vwgt, INTL *ewgt, flag_t num);
int checkBlock(csptr mata, int r, int s);
void ijv2csr(dim_t n, LONG nnz, dim_t *ii, dim_t *jj, COEF *val, INTL **ir, dim_t **jr, COEF **ar);
int checkHID(dim_t nlevel, int *levelindex, int *rperm, int *keyindex, int *key, dim_t n, INTL *ia, dim_t *ja);

/* int aplb(int nrow, int ncol, flag_t job, COEF* a, dim_t *ja, INTL *ia, COEF* b, int *jb, int *ib, COEF* c, int *jc, int *ic, int nzmax, int *iw); */
/* void csrcsc(int n, flag_t job, int ipos, COEF* a, dim_t *ja, INTL *ia, COEF* ao, dim_t *jao, INTL *iao); */
/* void csrcsc2(int n, int n2, flag_t job, int ipos, COEF* a, dim_t *ja, INTL *ia, COEF* ao, dim_t *jao, INTL *iao); */

void csr2csc(dim_t n, flag_t job, COEF* a, dim_t *ja, INTL *ia, COEF* ao, dim_t *jao, INTL *iao);
int aplb(dim_t nrow, dim_t ncol, flag_t job, COEF* a, dim_t *ja, INTL *ia, COEF* b, dim_t *jb, INTL *ib, COEF* c, dim_t *jc, INTL *ic, INTL nzmax, dim_t *iw);

void CSRrownormINF(csptr m, REAL *normtab);
void CSRrownorm1(csptr m, REAL *normtab);
void CSR_NoDiag_rownorm1(csptr m, REAL *normtab);
void CSRrownorm2(csptr m, REAL *normtab);
void CSR_NoDiag_rownorm2(csptr m, REAL *normtab);
void CSRcolnormINF(csptr m, REAL *normtab);
void CSRcolnorm1(csptr m, REAL *normtab);
void CSR_NoDiag_colnorm1(csptr m, REAL *normtab);
void CSRcolnorm2(csptr m, REAL *normtab);
void CSR_NoDiag_colnorm2(csptr m, REAL *normtab);

#ifdef DEBUG_M
void checkCSR(dim_t n, INTL *ia, dim_t *ja, COEF *a, flag_t numflag);
#endif

INTS HIPS_Fgmresd_CS_D1(flag_t verbose, REAL tol, dim_t itmax, 
		       csptr A, csptr L,
		       PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_Fgmresd_CS_D2(flag_t verbose, REAL tol, dim_t itmax, 
		       csptr A, csptr L, COEF *D,
		       PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_PCG_CS_D1(flag_t verbose, REAL tol, dim_t itmax, 
		       csptr A, csptr L,
		       PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_PCG_CS_D2(flag_t verbose, REAL tol, dim_t itmax, 
		       csptr A, csptr L, COEF *D,
		       PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);


void PrecInfo_AddNNZ(PrecInfo* info, INTL nnz);
void PrecInfo_SetNNZA(PrecInfo* info, INTL nnz);
void PrecInfo_SubNNZ(PrecInfo* info, INTL nnz);
void PrecInfo_Print(PrecInfo* info, int proc_id);
void PrecInfo_PrintMax(REAL ratio_max, REAL peakratio_max, int proc_id);

double dwalltime();

#ifdef DEBUG_M
#define DEBUG_M_PRINT 
#endif

#define fprintfv(level, stream, args...) { if (option->verbose >= (level)) fprintf(stream, args); }
#define printfv(level, args...) { if (option->verbose >= (level)) printf(args); }

#ifdef DEBUG_M_PRINT
#define fprintfd(stream, args...) { fprintf(stream, args); }
#define printfd(args...) { printf(args); }
#else
#define fprintfd(stream, args...) {  }
#define printfd(args...) {  }
#endif

#define PRINT_ERROR

#ifdef PRINT_ERROR
#define fprintferr(stream, args...) { fprintf(stream, args); }
#define printferr(args...) { printf(args); }
#else
#define fprintfd(stream, args...) {  }
#define printfd(args...) {  }
#endif

#ifdef DEBUG_M_PRINT
/* PRINTODO */

#ifndef PARALLEL
#define PRINT_MSG(args...) fprintfv(5, stdout, args)
#else
#define PRINT_MSG(args...) if (DBL->proc_id == 0) { fprintfv(5, stdout, args); }
#endif

/* PRINTODO */
#ifndef PARALLEL
#define PRINTALL_MSG(arg1, args...) fprintf(stdout, arg1, args);
#else
#define PRINTALL_MSG(arg1, args...) { if (DBL == NULL) fprintf(stdout,  "Proc : x : " arg1, args); else fprintf(stdout,  "Proc : %d : " arg1, DBL->proc_id, args); }
#endif

#else

#define PRINT_MSG(args...) ;
#define PRINTALL_MSG(arg1, args...) ;

#endif

/****/

#ifndef PARALLEL
#define PRINT_TIME(level, msg,time) fprintfv(level, stdout, msg, (time))
#else
#define PRINT_TIME(level, msg,time) fprintfv(level, stdout, "Proc %d : " msg, DBL->proc_id, (time))
#endif

#endif 
