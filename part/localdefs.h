/* @authors J. GAIDAMOUR, P. HENON */

#if defined(IBM) || defined(SP2) || defined(RS6000)
#define wreadmtc wreadmtc
#define wreadmtc64 wreadmtc64
#define userread userread  
#define nod2dom nod2dom
#define roscal roscal
#define coscal coscal
#define Fcsrcsc fcsrcsc
#define Faplb   faplb
#define Fcsrcscc fcsrcscc
#define Faplbc   faplbc
#define expnddom expnddom
#define dse3 dse3
#define gen5pt gen5pt
#elif defined(SGI)
#define wreadmtc wreadmtc_
#define wreadmtc64 wreadmtc64_
#define userread userread_  
#define nod2dom nod2dom_
#define roscal roscal_
#define coscal coscal_
#define Fcsrcsc fcsrcsc_
#define Faplb   faplb_
#define Fcsrcscc fcsrcscc_
#define Faplbc   faplbc_
#define expnddom expnddom_
#define dse3 dse3_
#define gen5pt gen5pt_
#elif defined(CRAY)
#define wreadmtc WREADMTC
#define wreadmtc64 WREADMTC64
#define userread USERREAD
#define nod2dom NOD2DOM
#define roscal ROSCAL
#define coscal COSCAL
#define Fcsrcsc FCSRCSC
#define Faplb   FAPLB
#define Fcsrcscc FCSRCSCC
#define Faplbc   FAPLBC
#define expnddom EXPNDDOM
#define dse3 DSE3
#define gen5pt GEN5PT
#else
#define wreadmtc wreadmtc_
#define wreadmtc64 wreadmtc64_
#define userread userread_
#define nod2dom nod2dom_
#define roscal roscal_
#define coscal coscal_
#define Fcsrcsc fcsrcsc_
#define Faplb   faplb_
#define Fcsrcscc fcsrcscc_
#define Faplbc   faplbc_
#define expnddom expnddom_
#define dse3 dse3_
#define gen5pt gen5pt_
#endif

/* for outer and inner functions */
extern void userread(char *fname, int *len, int *job, int *n, INTL
                      *nnz, REAL *a, int *ja, INTL *ia, int *nrhs,
                      REAL *rhs, int *ierr); 
extern void wreadmtc(int *nmax, int *nzmax, int *job, char *fname ,int
                     *len, REAL *a, int *ja, int *ia, REAL *rhs, 
		     int *nrhs, char *guesol, int *nrow, int *ncol,
		     int *nnz, char *title, char *key, char *type, int *ierr);

extern void wreadmtc64(int64_t *nmax, int64_t *nzmax, int64_t *job, int64_t
                     *len, REAL *a, int64_t *ja, int64_t *ia, REAL *rhs, 
		     int64_t *nrhs, char *guesol, int64_t *nrow, int64_t *ncol,
		     int64_t *nnz, char *title, char *key, char *type, int64_t *ierr, char *fname);

extern void nod2dom(int *n, int *ndom, int *nodes, int *pointer, int
		    *map, int *mapsize, int *ier);
extern void roscal(int *n, int *job, int *nrm, REAL *a, int *ja, int
		   *ia, REAL *diag, REAL *b, int *jb, int *ib, int
		   *ierr);
extern void coscal(int *n, int *job, int *nrm, REAL *a, int *ja, int
		   *ia, REAL *diag, REAL *b, int *jb, int *ib, int
		   *ierr);
extern void Fcsrcsc(int *n, int *job, int *ipos, REAL *a, int *ja,
		   int *ia, REAL *ao, int *jao, int *iao);
extern void Faplb(int *nrow, int *ncol, int *job, REAL *a, int *ja,
		 int *ia, REAL *b, int *jb, int *ib, REAL *c, int
		 *jc, int *ic, int *nnzmax, int *iw, int *ierr);
extern void Fcsrcscc(int *n, int *job, int *ipos, COEF *a, int *ja,
		   int *ia, COEF *ao, int *jao, int *iao);
extern void Faplbc(int *nrow, int *ncol, int *job, COEF *a, int *ja,
		 int *ia, COEF *b, int *jb, int *ib, COEF *c, int
		 *jc, int *ic, int *nnzmax, int *iw, int *ierr);
extern  void expnddom(int *, int *,  int *,  int *, int *, int *, int 
		       *, int *, int *, int *);
extern void dse3(int *n,int *ja,int *ia,int *ndom,int *dom,int *domptr,int *mask,int *riord,int *jwk,int *link);
extern void gen5pt(int *nx,int *ny,int *nz, REAL *a,int *ja,int *ia, int *iau);


/*-----------------------------------------------------------------------*/

