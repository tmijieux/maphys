/* @authors J. GAIDAMOUR, P. HENON */

#ifndef _BASE_HEADER_
#define _BASE_HEADER_

#include "type.h"

/* #define CBLAS */

/* #define   BLAS_DOT(N, X, incX, Y, incY)  1 */
/* #define   BLAS_NRM2(N, X, incX)  1 */
/* #define   BLAS_ASUM(N, X, incX)  1 */
/* #define   BLAS_SWAP(N, X, incX, Y, incY)   */
/* #define   BLAS_COPY(N, X, incX, Y, incY)   */
/* #define   BLAS_AXPY(N, alpha, X, incX, Y, incY)   */
/* #define   BLAS_SCAL(N, alpha, X, incX)   */
/* #define   BLAS_GEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY)   */
/* #define   BLAS_GBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY)   */
/* #define   BLAS_TRMV(Uplo, TransA, Diag, N, A, lda, X, incX)   */
/* #define   BLAS_TBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX)   */
/* #define   BLAS_TPMV(Uplo, TransA, Diag, N, Ap, X, incX)   */
/* #define   BLAS_TRSV(Uplo, TransA, Diag, N, A, lda, X, incX)   */
/* #define   BLAS_TBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX)   */
/* #define   BLAS_TPSV(Uplo, TransA, Diag, N, Ap, X, incX)   */
/* #define   BLAS_SYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY)   */
/* #define   BLAS_SBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY)   */
/* #define   BLAS_SPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY)   */
/* #define   BLAS_GER(M, N, alpha, X, incX, Y, incY, A, lda)   */
/* #define   BLAS_SYR(Uplo, N, alpha, X, incX, A, lda)   */
/* #define   BLAS_SPR(Uplo, N, alpha, X, incX, Ap)   */
/* #define   BLAS_SYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda)   */
/* #define   BLAS_SPR2(Uplo, N, alpha, X, incX, Y, incY, A)   */
/* #define   BLAS_GEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc)   */
/* #define   BLAS_SYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc)   */
/* #define   BLAS_SYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc)   */
/* #define   BLAS_SYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc)   */
/* #define   BLAS_TRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb)   */
/* #define   BLAS_TRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb)   */

#if defined(TYPE_REAL)

#if defined(PREC_DOUBLE)

#define BLAS_DOT BLAS_DDOT
#define BLAS_NRM2 BLAS_DNRM2
#define BLAS_SWAP BLAS_DSWAP
#define BLAS_COPY BLAS_DCOPY
#define BLAS_AXPY BLAS_DAXPY
#define BLAS_SCAL BLAS_DSCAL
#define BLAS_SCAL2 BLAS_DSCAL
#define BLAS_GEMV BLAS_DGEMV
#define BLAS_GBMV BLAS_DGBMV
#define BLAS_TRMV BLAS_DTRMV
#define BLAS_TBMV BLAS_DTBMV
#define BLAS_TPMV BLAS_DTPMV
#define BLAS_TRSV BLAS_DTRSV
#define BLAS_TBSV BLAS_DTBSV
#define BLAS_TPSV BLAS_DTPSV
#define BLAS_SYMV BLAS_DSYMV
#define BLAS_SBMV BLAS_DSBMV
#define BLAS_SPMV BLAS_DSPMV
#define BLAS_GEMM BLAS_DGEMM
#define BLAS_SYMM BLAS_DSYMM
#define BLAS_SYRK BLAS_DSYRK
#define BLAS_SYR2K BLAS_DSYR2K
#define BLAS_TRMM BLAS_DTRMM
#define BLAS_TRSM BLAS_DTRSM

#elif defined(PREC_SIMPLE)

#define BLAS_DOT BLAS_SDOT
#define BLAS_NRM2 BLAS_SNRM2
#define BLAS_SWAP BLAS_SSWAP
#define BLAS_COPY BLAS_SCOPY
#define BLAS_AXPY BLAS_SAXPY
#define BLAS_SCAL BLAS_SSCAL
#define BLAS_SCAL2 BLAS_SSCAL
#define BLAS_GEMV BLAS_SGEMV
#define BLAS_GBMV BLAS_SGBMV
#define BLAS_TRMV BLAS_STRMV
#define BLAS_TBMV BLAS_STBMV
#define BLAS_TPMV BLAS_STPMV
#define BLAS_TRSV BLAS_STRSV
#define BLAS_TBSV BLAS_STBSV
#define BLAS_TPSV BLAS_STPSV
#define BLAS_SYMV BLAS_SSYMV
#define BLAS_SBMV BLAS_SSBMV
#define BLAS_SPMV BLAS_SSPMV
#define BLAS_GEMM BLAS_SGEMM
#define BLAS_SYMM BLAS_SSYMM
#define BLAS_SYRK BLAS_SSYRK
#define BLAS_SYR2K BLAS_SSYR2K
#define BLAS_TRMM BLAS_STRMM
#define BLAS_TRSM BLAS_STRSM

#else
#error PREC_DOUBLE or PREC_SIMPLE undefined
#endif




#elif defined(TYPE_COMPLEX)





#if defined(PREC_DOUBLE)

#define BLAS_DOT BLAS_ZDOTC
#define BLAS_NRM2 BLAS_DZNRM2
#define BLAS_SWAP BLAS_ZSWAP
#define BLAS_COPY BLAS_ZCOPY
#define BLAS_AXPY BLAS_ZAXPY
#define BLAS_SCAL BLAS_ZSCAL
#define BLAS_SCAL2 BLAS_ZDSCAL
#define BLAS_GEMV BLAS_ZGEMV
#define BLAS_GBMV BLAS_ZGBMV
#define BLAS_TRMV BLAS_ZTRMV
#define BLAS_TBMV BLAS_ZTBMV
#define BLAS_TPMV BLAS_ZTPMV
#define BLAS_TRSV BLAS_ZTRSV
#define BLAS_TBSV BLAS_ZTBSV
#define BLAS_TPSV BLAS_ZTPSV
#define BLAS_SYMV BLAS_ZSYMV
#define BLAS_SBMV BLAS_ZHBMV
#define BLAS_SPMV BLAS_ZHPMV
#define BLAS_GEMM BLAS_ZGEMM
#define BLAS_SYMM BLAS_ZSYMM
#define BLAS_SYRK BLAS_ZSYRK
#define BLAS_SYR2K BLAS_ZSYR2K
#define BLAS_TRMM BLAS_ZTRMM
#define BLAS_TRSM BLAS_ZTRSM

#elif defined(PREC_SIMPLE)

#define BLAS_DOT BLAS_CDOTU
#define BLAS_NRM2 BLAS_SCNRM2
#define BLAS_SWAP BLAS_CSWAP
#define BLAS_COPY BLAS_CCOPY
#define BLAS_AXPY BLAS_CAXPY
#define BLAS_SCAL BLAS_CSCAL
#define BLAS_SCAL2 BLAS_CSSCAL /* TODO, nom*/
#define BLAS_GEMV BLAS_CGEMV
#define BLAS_GBMV BLAS_CGBMV
#define BLAS_TRMV BLAS_CTRMV
#define BLAS_TBMV BLAS_CTBMV
#define BLAS_TPMV BLAS_CTPMV
#define BLAS_TRSV BLAS_CTRSV
#define BLAS_TBSV BLAS_CTBSV
#define BLAS_TPSV BLAS_CTPSV
#define BLAS_SYMV BLAS_CSYMV
#define BLAS_SBMV BLAS_CHBMV
#define BLAS_SPMV BLAS_CHPMV
#define BLAS_GEMM BLAS_CGEMM
#define BLAS_SYMM BLAS_CSYMM
#define BLAS_SYRK BLAS_CSYRK
#define BLAS_SYR2K BLAS_CSYR2K
#define BLAS_TRMM BLAS_CTRMM
#define BLAS_TRSM BLAS_CTRSM

#else
#error PREC_DOUBLE or PREC_SIMPLE undefined
#endif


#else
#error TYPE_REAL or TYPE_COMPLEX undefined
#endif

#endif /* _BASE_HEADER_ */
