#include "prec.h"
#include "matrix.h"

struct PhidalPrec_ {
  flag_t symmetric; 
  dim_t dim; /** The dimension of the  system  to be preconditioned **/
  int_t levelnum;
  int_t forwardlev;
  
  Matrix* LU;
/*   PhidalMatrix *L; */
/*   PhidalMatrix *U; */

#if defined(ILUT_WHOLE_SCHUR) || defined(ILUT_SOLVE)
#error sss
  csptr csL;
  csptr csU;
#endif

  COEF *D;
  Matrix *EF;
/*   PhidalMatrix *E; */
/*   PhidalMatrix *F; */
/*   PhidalMatrix *S; */
/*   PhidalMatrix *B; */
  Matrix *S;
  Matrix *B;

  /*PhidalMatrix *C;  OPTIMREC forward */
  PhidalPrec *prevprec;
  PhidalPrec *nextprec;
  flag_t schur_method;  /** Indicate the smoothing method in the multi-level;
			  0 := no smoothing; in this case the schur complement S is not stored
			  1 := iteration in the schur complement with storage of the schur complement
			  2 := iteration in the schur complement with implicit schur complement (schur not stored) */

  flag_t pivoting;  /** 1 := column pivoting in ILUTP */
  dim_t *permtab;

  /* REAL tol_schur; */
  
  PrecInfo* info;
};
