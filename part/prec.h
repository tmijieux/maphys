/* @authors J. GAIDAMOUR */

#ifndef PREC_H
#define PREC_H

#include "matrix.h"

typedef struct PhidalPrec_ PhidalPrec;
typedef struct PhidalDistrPrec_ PhidalDistrPrec;

/* /!\ #define PARALLEL must be set before #include "prec.h" */





/* #include "phidal_struct.h" */
/* #include <mpi.h> */
/* #include "phidal_parallel_struct.h" */
/* #include "db_parallel_struct.h" */

/* #include "phidal_parallel_struct_sub.h" */
/* #include "db_parallel_struct_sub.h" */

/* #include "solver.h" */

typedef struct Prec_  Prec;
struct Prec_ {
  flag_t type; /* temporaire, il n'en restera plus qu'un ... */

  flag_t symmetric; 
  dim_t dim;
  int_t levelnum;
  int_t forwardlev;

  Matrix* LU;

  Matrix* EF;

  Matrix* EF_DB; /* tmp, a suppr */

  Matrix* B;

  Matrix* S;

  Prec *prevprec;
  Prec *nextprec;

  flag_t schur_method;

  PrecInfo* info;

  /* scalar : */
  flag_t pivoting;  /** 1 := column pivoting in ILUTP */
  dim_t *permtab;

  /*tmp*/
  PhidalMatrix *phidalS;
  PhidalPrec   *phidalPrec;

};

/* TMP */
typedef struct DistrPrec_  DistrPrec;
struct DistrPrec_ {
  flag_t type; /* temporaire, il n'en restera plus qu'un ... */

  flag_t symmetric; 
  dim_t dim;
  int_t levelnum;
  int_t forwardlev;

  Matrix* LU;

  Matrix* EF;

  Matrix* EF_DB; /* tmp, a suppr */

  Matrix* B;

  Matrix* S;

  DistrPrec *prevprec;
  DistrPrec *nextprec;

  flag_t schur_method;

  PrecInfo* info;

  /* scalar : */
  flag_t pivoting;  /** 1 := column pivoting in ILUTP */
  dim_t *permtab;

  /*tmp*/
  PhidalDistrMatrix *phidalS;
  PhidalDistrPrec   *phidalPrec;

};


#undef PREC_L
#undef PREC_U
#undef PREC_EDB
#undef PREC_FDB
#undef PREC_SL
#undef PREC_SU

#define PREC_LU_BLOCK(P) (P)->M_BLOCK(LU)
#define PREC_L_BLOCK(P)  (P)->M_BLOCK(LU).L
#define PREC_U_BLOCK(P)  (P)->M_BLOCK(LU).U

#define PREC_LU_SCAL(P) (P)->M_SCAL(LU)
#define PREC_L_SCAL(P)  (P)->M_SCAL(LU).L
#define PREC_U_SCAL(P)  (P)->M_SCAL(LU).U


/** OIMBE  A VIRER*/
#define PREC_EF(P) (P)->M_SCAL(EF)
#define PREC_E(P)  (P)->M_SCAL(EF).L
#define PREC_F(P)  (P)->M_SCAL(EF).U
/*** ===> Prendre des notations coherentes : ***/
#define PREC_EF_SCAL(P) (P)->M_SCAL(EF)
#define PREC_E_SCAL(P)  (P)->M_SCAL(EF).L
#define PREC_F_SCAL(P)  (P)->M_SCAL(EF).U


/** OIMBE  A VIRER*/
#define PREC_EFDB(P) (P)->M_BLOCK(EF_DB)
#define PREC_EDB(P) (P)->M_BLOCK(EF_DB).L
#define PREC_FDB(P) (P)->M_BLOCK(EF_DB).U
/*** ===> Prendre des notations coherentes : ***/
#define PREC_EF_BLOCK(P) (P)->M_BLOCK(EF_DB)
#define PREC_E_BLOCK(P) (P)->M_BLOCK(EF_DB).L
#define PREC_F_BLOCK(P) (P)->M_BLOCK(EF_DB).U

#define PREC_S_BLOCK(P)  (P)->M_BLOCK(S)
#define PREC_SL_BLOCK(P) (P)->M_BLOCK(S).L
#define PREC_SU_BLOCK(P) (P)->M_BLOCK(S).U

#define PREC_S_SCAL(P)  (P)->M_SCAL(S).L

/** OIMBE  A VIRER*/
#define PREC_B(P)  (P)->M_SCAL(B).L
/** Unification **/
#define PREC_B_SCAL(P)  (P)->M_SCAL(B).L

#ifndef PARALLEL

#if defined(SYMMETRIC)
#define _MATRIX_MLILUPrec     MATRIX_MLICCPrec
#elif defined(UNSYMMETRIC)
#define _MATRIX_MLILUPrec     MATRIX_MLILUPrec
#endif

#else /* PARALLEL */

#if defined(SYMMETRIC)
#define _MATRIX_MLILUPrec     DISTRMATRIX_MLICCPrec
#elif defined(UNSYMMETRIC)
#define _MATRIX_MLILUPrec     DISTRMATRIX_MLILUPrec
#endif

#endif /* PARALLEL */


#endif


