# Cmake build directory
BUILDDIR=spack-build

# Source directories
SUBDIRS:=common dm sm cgc sds hds dds part hybrid \
	 toolkit unittest gendistsys examples\
	 slatec packcg packgmres includ

all:
	@echo "The installation must be made in the cmake build directory"
	@echo "See README"

## Generate the tags
TAGS :
	-etags $(foreach d,$(SUBDIRS) include,\
	$(wildcard $(d)/*.f $(d)/*.F $(d)/*.c $(d)/*.h \
	$(d)/xmph_*.F90 $(d)/mph_*.F90 $(d)/maphys_*.F90 ))

## Build documentation
doc: doc/users_guide.pdf
doc/users_guide.pdf :
	$(MAKE) -C doc/userguide

## Clean
clean:
	-$(RM) TAGS
	-$(RM) -rf $(BUILDDIR)
	-$(RM) spack-build*
	$(MAKE) -C doc/userguide clean

## Tests
check:
	$(MAKE) -C $(BUILDDIR) test

test: check
