\section{Application Programming Interface}

\subsection{General}

\subsubsection{Maphys instance}

\textbf{mphs} is the structure that will describe one linear system to
solve.  You have to use one structure for each linear system to solve.

\subsubsection{Arithmetic versions}

The package is  available in the four main arithmetics  : REAL, DOUBLE
PRECISION, COMPLEX, DOUBLE COMPLEX.

\subsubsection{Version number}

\textbf{mphs\%VERSION}  (string) gives  information about  the current
\maphys{}  version.    It  is   set  during  the   intialization  step
(mphs\%JOB=-1)

\subsubsection{Control of the four main phases}

The  main  action performed  by  \maphys{}  (\emph{i.e.} when  calling
\verb!*MPH_maphys_driver(mph)!) is  specified by the integer  mphs\%JOB.  The
user  must set  it on  all processes  before each  call to  \maphys{}.
Possible values of mphs\%JOB are :
\begin{itemize}
\item \textbf{-1} initializes  the instance. It must  be called before
  any other call to \maphys{}.  \maphys{} will nullify all pointers of
  the  structure,  and  fill  the  controls  arrays  (mphs\%ICNTL  and
  mphs\%RCNTL) with default values.
\item  \textbf{-2}  destroys the  instance.  \maphys{}  will free  its
  internal memory.
\item \textbf{ 1}  perfoms the analysis. In this  step, \maphys{} will
  pretreat  the input  matrix, compute  how to  distribute the  global
  system, and distribute the global system into local systems.
\item  \textbf{ 2}  performs the  factorization assuming  the data  is
  distributed in  the form of  local systems. In this  step, \maphys{}
  will factorize the interior of the local matrices, and compute their
  local Schur complements.
\item  \textbf{  3}  performs   the  preconditioning.  In  this  step,
  \maphys{} will  compute the  preconditioner of the  Schur complement
  system.
\item \textbf{ 4} performs the  solution. In this step, \maphys{} will
  solve  the linear  system.  Namely,  \maphys{} will  reformulate the
  right-hand-side of  the input  linear system (mphs\%RHS)  into local
  system right-hand-sides, solve the Schur complement system(s), solve
  the local sytem
% and gather local solutions on proces with MPI rank 0 to form the global solution.
\item \textbf{ 5} combines the actions from JOB=1 to JOB=3.
\item \textbf{ 6} combines the actions from JOB=1 to JOB=4. 

\end{itemize}

\subsubsection{Control of parallelism}
\label{parallelism}
\begin{itemize}
\item \textbf{mphs\%COMM}  (integer) is  the MPI communicator  used by
  \maphys{}.  It must  be set to a valid MPI  communicator by the user
  before  the initialization  step (mphs\%JOB  =  -1) ,  and must  not
  changed later.
\item \emph{In the current version, when the input system is initially
  centralized   on  the   host  (\ref{itm:icntl43}   =  1),   the  MPI
  communicator must have a number of process that is a power of 2.}
\item \emph{In the current version,  the working host process is fixed
  to  the   process  with   MPI  rank  0   according  to   the  chosen
  communicator.}
\end{itemize}


\subsection{Input matrix, right-hand side and solution vectors}
\label{sec:input-matrix}

\maphys{} aims at solving a linear system of the form ${\cal A} x = b$,
where ${\cal A}$ is a square real or complex non singular sparse
matrix, $b$ is a given right-hand side (RHS) vector, and $x$ is the
solution vector to be computed.

Section~\ref{sec:matrix-type} presents the types of matrices
\maphys{} can handle (through \textbf{mphs\%SYM} control parameter)
while Section~\ref{sec:matrix-format} details the matrix (and
subsequent RHS and solution vectors) formats \maphys{} can handle.

\subsubsection{Matrix type}
\label{sec:matrix-type}
\begin{itemize}
\item \textbf{mphs\%SYM} (integer) specifies the input matrix type.
  It is accessed during the analysis and the solve steps (mphs\%JOB == -1 and  mphs\%JOB == 4 respectively).
  \maphys{} does not modify its value during its execution.
  Possible values of mphs\%SYM are :
  \begin{itemize}
    \item \textbf{0 or SM\_SYM\_IsGeneral} means the input matrix is
      general, \emph{i.e.} no assumtion is made on his pattern.
    \item \textbf{1 or SM\_SYM\_IsSPD} means the input matrix is
      symmetric positive definite (SPD) in real arithmetic (s/d).  In
      complex arithmetic (z/c), this is an invalid value.
    \item \textbf{2 or SM\_SYM\_IsSymmetric} means the input matrix is
      symmetric (even in complex).
  \end{itemize}
\end{itemize}

%General matrices (\textbf{mphs\%SYM=0}) must be processed in symme

In  centralized input  mode (Section~\ref{sec:centralized-input}),  be
aware that in  the case of symmetric  matrices (\textbf{mphs\%SYM=1 or
  2}),  only ``half''  of the  matrix (including  diagonal) should  be
provided (because \maphys{} automatically fills the other off-diagonal
half).
% On the the contrary ... ~\ref{sec:distributed-input}


Unsymmetric   matrices  must   be   processed   as  general   matrices
(\textbf{mphs\%SYM=0}). Although  not recommended,  symmetric matrices
can also be  processed in this mode but all  coefficients must then be
filled (lower and upper coefficients) in that case.

In  the  present version,  \maphys{}  does  not support  Hermitian  or
Hermitian  Positive  Definite  matrices.  Such  matrices  have  to  be
processed as general matrices (\textbf{mphs\%SYM=0}).


 
\subsubsection{Matrix format}
\label{sec:matrix-format}

The matrix format is controlled by \ref{itm:icntl43}. \maphys{} handles
either a centralized assembled matrix format (\ref{itm:icntl43}=1) or
a distributed subdomain format (\ref{itm:icntl43}=2):
\begin{itemize}
\item \ref{itm:icntl43}=1: the input matrix and RHS are supplied
  \emph{centrally} on the host process (MPI process 0) in an
  \emph{assembled form}. In this case, during the analysis step,
  \maphys{} performs a domain decomposition and distributes the matrix
  on the MPI processes according to internal MaPHyS parallelization
  strategies. Section~\ref{sec:centralized-input} further describes
  the expected input.
\item \ref{itm:icntl43}=2: the input matrix and RHS are supplied in a
  \emph{distributed} way (each MPI process provides the entries of the
  matrix and RHS it has computed) in a \emph{subdomain form}
  (multiple MPI processes may compute part of a coefficient -- at
  their interface -- that are then summed up by \maphys). This
  interface shall be very convenient for users having a parallel
  distributed application that already performs a domain
  decomposition. Indeed, each MPI process can provide the local
  matrices and RHS it has assembled on the subdomain it is associated
  to. Section~\ref{sec:distributed-input} further describes the
  expected input.
\end{itemize}

\paragraph{Centralized assembled matrix input}
\label{sec:centralized-input}

In the centralized assembled matrix input mode (\ref{itm:icntl43}=1),
the matrix is provided in coordinate format through mphs\%SYM,
mphs\%N, mphs\%NNZ, mphs\%ROWS, mphs\%COLS and mphs\%VALUES.  These
components are accessed during the analysis step (until the matrix
gets distributed) and the solve step (to compute statistics) by the
host process. They are not altered by \maphys{}. In details:
\begin{itemize}
  \item \textbf{mphs\%SYM} (integer) specifies the type of symmetry (see Section \ref{sec:matrix-type})
  \item \textbf{mphs\%N  } (integer) specifies the order of the input matrix (hence $N > 0$)
  \item \textbf{mphs\%NNZ} (integer) specifies the number of the entries in the input matrix (hence $NNZ > 0$ )
  \item \textbf{mphs\%ROWS} (integer array of size NNZ) lists the row indices of the matrix entries
  \item \textbf{mphs\%COLS} (integer array of size NNZ) lists the column indices of the matrix entries
  \item \textbf{mphs\%VALUES} (Real/Complex array of size NNZ) lists the values  of the matrix entries
\end{itemize}

Note that, in the case of symmetric matrices (mphs\%SYM=1 or 2), only
half of the matrix should be provided.  For example, only the lower
triangular part of the matrix (including the diagonal) or only the
upper triangular part of the matrix (including the diagonal) can be
provided in ROWS, COLS, and VALUES.  More precisely, a diagonal
nonzero ${\cal A}_{ii}$ must be provided as $VALUES(k)={\cal A}_{ii}$,
$ROWS(k)=COLS(k)=i$, and a pair of off-diagonal nonzeros ${\cal
  A}_{ij}={\cal A}_{ji}$ must be provided either as ${\cal A}(k)={\cal
  A}_{ij}$ and $ROWS(k)=i$, $COLS(K)=j$ or vice-versa. Duplicate
entries are summed. In particular, this means that if both ${\cal
  A}_{ij}$ and ${\cal A}_{ji}$ are provided, they will be summed.

%More precisely, a diagonal  nonzero ${\cal A}_{ii}$ must  be  provided  as  ${\cal A}(k)= {\cal A}_{ii}$, $ROWS(k)=COLS(k)= i$,  and  a  pair  of  off-diagonal nonzeros ${\cal A}_{ij}={\cal A}_{ji}$ must be provided either as ${\cal A}(k)={\cal A}_{ij}$ and $ROWS(k)=i$, $COLS(K)=j$ or vice-versa. 
%Again duplicates entries are summed.  In particular, this means that if both ${\cal A}_{ij}$ and ${\cal A}_{ji}$ are provided, they will be summed.
 % \subsection Distributed assembled matrix input: -> Laisser en commentaire pour
 % le moment
 % \subsection Workspace parameters -> Laisser en commentaire

The RHS and solution vector are handled as follows:
\begin{itemize}
  \item \textbf{mphs\%RHS} (Real/Complex array of lenght mphs\%N $\times$ mphs\%NRHS) is the centralized right-hand-side of the linear system.
    It holds the right-hand side (RHS) allocated and filled by the user, on the host, before the solve step (mphs\%JOB == 4,6). \maphys{} does not alterate its value.
  \item \textbf{mphs\%NRHS} (integer) specifies the number of right-hand-sides, which is the number of columns in the matrix mphs\%RHS. The default value is 1.
  \item \textbf{mphs\%SOL} (Real/Complex array of lenght mphs\%N) is the centralized initial guess and/or the solution of the linear system. It must be allocated by the user before the solve step (mphs\%JOB == 4,6).
    \begin{itemize}
    \item Before the solve step, on the host, it holds the initial guess if the initial guess option is activated (mphs\%icntl(23) == 1).
    \item After the solve step, on the host, it holds the solution.
    \end{itemize}
  \item \textbf{mphs\%tolerances} (Real array of lenght
    mphs\%NRHS) should be set if you want to use multiple tolerances.
    Element $i$ of the array contains the wanted tolerance for RHS number
    $i$. If you want to use single tolerance or if you do not use multiple
    RHS, do not associate this pointer and set \ref{itm:rcntl21}.


  
\end {itemize}


\paragraph{Distributed subdomain interface (experimental and succeptible to change)}
\label{sec:distributed-input}

\begin{figure}
  \begin{subfigure}[b]{0.4\textwidth}
    \centering
    \includegraphics[width=\textwidth]{./figures/graph_example.png}
    \caption{Domain global ordering.}
    \label{fig:dd-global}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[width=\textwidth]{./figures/graph_example_local_order.png}
    \caption{Subdomain local orderings.}
    \label{fig:dd-local}
  \end{subfigure}
  \caption{Domain decomposition of a 6x5 grid performed by the application.}
  \label{fig:dd}
\end{figure}

The distributed subdomain input mode (\ref{itm:icntl43}=2) has been
especially designed for users who have a parallel distributed
application that already performs a domain decomposition before
calling \maphys. At this stage, it is functional, but the API is not
stabilized and hence succeptible to change in future versions.
We make the following assumptions:
\begin{enumerate}
\item the user has performed a domain decomposition with vertex
  separators such as in the example of Figure~\ref{fig:dd-global};
\item one MPI process is associated to one domain and assembles the
  coefficients of the matrix and the RHS arising from its domain (4
  processes in the example of Figure~\ref{fig:dd});
\item each MPI process orders contiguously its local interior vertices
  prior to its interface vertices ranging from 1 to the local number
  of degrees of freedom (12 for subdomain 1 of
  Figure~\ref{fig:dd-local}).
\end{enumerate}

In this context, instead of providing ${\cal A}$, $x$ and $b$ global
data to \maphys, the user provides:
\begin{enumerate}
\item ${\cal A}_i$, $x_i$ and $b_i$ local data associated to
  domain $i$, where
$$
{\cal A}_i =  \begin{pmatrix}  {\cal A}_{{\cal I}_i{\cal I}_i} &  {\cal A}_{{\cal I}_i  \Gamma_i}  \\
              {\cal A}_{\Gamma_i {\cal I}_i}  &  {\cal A}_{\Gamma_i \Gamma_i}  \end{pmatrix} ,
{b}_i =  \begin{pmatrix} b_{{\cal I}_i}  \\ b_{\Gamma_i}
\end{pmatrix}
$$ consistently with Section~\ref{sec:intro-hierarchical}. Each
process hence fills the mphs\%SYM, mphs\%N, mphs\%NNZ, mphs\%ROWS,
mphs\%COLS, mphs\%VALUES, mphs\%RHS and possibly mphs\%SOL as in
~\ref{sec:centralized-input} but with respect to the ${\cal A}_i$,
$x_i$ and $b_i$ local data it has assembled (\emph{e.g.}, mphs\%N = 12
for Domain 1 in Figure~\ref{fig:dd-local}); in particular, the
numbering of rows mphs\%ROWS and columns mphs\%COLS are local
consistently with the example provided in Figure~\ref{fig:dd-local}.
\item the interconnectivity pattern.
\end{enumerate}

In the current \maphys{} API, the interconnectivity pattern is provided
to \maphys{} in two steps. First we provide some data structures
(allocated by the user) and then we call the dedicated
\verb?XMPH_distributed_interface? \maphys{} helper routine that provides
those data structures to \maphys{} so that the solver can keep track of
them (and rearrange them according to some internal constraints).

These data structures, which the user must specify on each process,
are divided into the three categories:
\begin{enumerate}
\item related to the local matrix:
\begin{itemize}
\item \texttt{myndof}: number of rows (columns) in the local matrix.
\end{itemize}
\item related to the interface:
\begin{itemize}
\item \texttt{mysizeIntrf}: size of the local interface (:= size of local Schur).
\item \texttt{myinterface}: global numbering of the local interface
\end{itemize}
\textit{Warning:} the  ordering of  the interface  nodes
into  \texttt{myinterface} has  to  match the  ordering of  the
interface nodes into the local matrix.
\item related to the subdomain's neighbors:
\begin{itemize}
\item \texttt{mynbvi}: number of subdomains neighbor of the current subdomain,
\item \texttt{myindexVi(1:mynbvi)}:  array  of  indices   of  the  neighbor  MPI
processes, holding a neighbor subdomain,
\item \texttt{myptrindexVi(1:mynbvi+1)}:  index  of  the  first  vertex  on  the
  interface shared with neighbor sudomains in \texttt{mypindexintrf(:)},
\item \texttt{myindexintrf(1:lenindintrf)}:  list  of   the  interface  vertices
shared with neighbors  (in the same order  than in \texttt{myindexVi(:)}).
\begin{verbatim}
Example: for a neighbor v  in myindexVi(:), the node  shared are
given by myindexintrf(myptrindexVi(v):myptrindexVi(v+1)-1)
\end{verbatim}
\end{itemize}
\end{enumerate}

In the example of Figure~\ref{fig:dd-local}, assuming each domain number
$i$ is mapped on the MPI process of rank $i-1$, the resulting data
structures are as follows (arrays are written between square brackets, and
elements within arrays are separated with commas):

\begin{center}
\begin{tabular}{lll}
Data structure (user input) & Domain 1 & Domain 2\\
\hline
\texttt{myndof} & 12 & 9\\
\texttt{mysizeIntrf} & 6 & 5\\
\texttt{myinterface} & [13, 14, 15, 16, 10, 4] & [18, 17, 16, 10, 4]\\
\texttt{mynbvi} & 3 & 3\\
\texttt{myindexVi(:)} & [1, 2, 3] & [0, 2, 3]\\
\texttt{myptrindexVi(:)} & [1, 4, 8, 9] & [1, 4, 5, 8]\\
\texttt{myindexintrf(:)} & [4, 5, 6, 1, 2, 3, 4, 4] & [3, 4, 5, 3, 1, 2, 3]\\
 &  & \\
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{lll}
Data structure  (user input) & Domain 3 & Domain 4\\
\hline
\texttt{myndof} & 12 & 9\\
\texttt{mysizeIntrf} & 6 & 5\\
\texttt{myinterface} & [13, 14, 15, 16, 22, 28] & [18, 17, 16, 22, 28]\\
\texttt{mynbvi} & 3 & 3\\
\texttt{myindexVi(:)} & [0, 1, 3] & [0, 1, 2]\\
\texttt{myptrindexVi(:)} & [1, 5, 6, 9] & [1, 2, 5, 8]\\
\texttt{myindexintrf(:)} & [1, 2, 3, 4, 4, 4, 5, 6] & [3, 1, 2, 3, 3, 4, 5]\\
 &  & \\
\end{tabular}
\end{center}

Once these data structures have been filled on each process, the
\verb?XMPH_distributed_interface? \maphys{} helper routine
must be called. In Fortran, the call is performed as follows:
\lstset{basicstyle= \ttfamily,commentstyle=\color{green!40!black},stringstyle=\color{red!40!black},keywordstyle=\bfseries \color{blue!50!black},showstringspaces=false,frame=single,frameround=ffft,numbers=left,numberstyle=\tiny,firstnumber=last,breaklines=true,language=fortran,label= ,caption= }
\begin{lstlisting}
  CALL XMPH_distributed_interface(mphs, myndof, mysizeintrf, myinterface, mynbvi, myindexvi, myptrindexvi, myindexintrf)
\end{lstlisting}
where the X character of the subroutine has to be replaced by S,D,C or
Z, depending  on  the  chosen  arithmetic, and  mphs  is  the  MaPHyS
instance.

The function to be called in C is:
\lstset{basicstyle= \ttfamily,commentstyle=\color{green!40!black},stringstyle=\color{red!40!black},keywordstyle=\bfseries \color{blue!50!black},showstringspaces=false,frame=single,frameround=ffft,numbers=left,numberstyle=\tiny,firstnumber=last,breaklines=true,language=C,label= ,caption= }
\begin{lstlisting}
  xmph_distributed_interface_c(XMPH_maphys_t_c* maphys_par, int myndof, int mysizeintrf, int* myinterface, int mynbvi, int* myindexvi, int* myptrindexvi, int* myindexintrf)
\end{lstlisting}


%% Optionally, one may also read these domain data from files on each MPI
%% process, and set manually ICNTL(43) to 2.
%% \lstset{basicstyle= \ttfamily,commentstyle=\color{green!40!black},stringstyle=\color{red!40!black},keywordstyle=\bfseries \color{blue!50!black},showstringspaces=false,frame=single,frameround=ffft,numbers=left,numberstyle=\tiny,firstnumber=last,breaklines=true,language=fortran,label= ,caption= }
%% \begin{lstlisting}
%%   open(unit=runit,file=domfile,action="read")
%%   Call MPH_domain_read_and_fill(mphs\%lc_domain, mphs\%comm, runit, iinfo)
%%   mphs\%icntl(43)=2
%% \end{lstlisting}

%% The file format is given by the following example:
%% \lstset{basicstyle= \ttfamily,commentstyle=\color{green!40!black},stringstyle=\color{red!40!black},keywordstyle=\bfseries \color{blue!50!black},showstringspaces=false,frame=single,frameround=ffft,numbers=left,numberstyle=\tiny,firstnumber=last,breaklines=true,language=sh,label= ,caption= }
%% \begin{lstlisting}
%%  ndof=  36
%%  sizeIntrf=  6
%%  myinterface= 31 32 33 34 35 36
 
%%  nbVi=  1
%%  indexVi(:)=  1 
%%  ptrIndexVi(:)=  1 7 
%%  IndexIntrf(:)=  1 2 3 4 5 6
%% \end{lstlisting}


% \subsection{Finite element interface(experimental)}
% 
% \subsubsection{How to use it}
% 
% \maphys{} provides a distributed interface according to finite element interface. The control of which interface you want to use is define by \ref{itm:icntl43}. In order to use the finite element interface the user has to call a routine called xmph\_create\_domain defined as follows before the analysis step:
% \begin{verbatim}
% xmph_create_domain(mphs, myndof, myndofinterior, myndofintrf,
%                      lenindintrf, mysizeintrf,gballintrf, mynbvi,
%                      myindexvi, myptrindexvi, myindexintrf,
%                      myinterface,myndoflogicintrf, mylogicintrf, weight)
% 
% \end{verbatim}
% 
% 
% The parameters are defined as follow :
% \begin{itemize}
% \item $mphs$ : the \maphys{} instance
% \item $myndof$ : integer equal to the degree of freedom of the entire domain (interface + interior);
% \item $myndofinterior$ : integer equal to the degree of freedom of the domain interior;
% \item $myndofintrf$ : integer equal to the degree of freedom of the domain interface;
% \item $lenindintrf$ : integer equal to the size of the array $myindexintrf$
% \item $mysizeintrf$ :  integer equal to the size of the domain interface;
% \item $gballintrf$ : integer equal to the total size of each interfaces in each processus;
% \item $mynbvi$ : integer equal to the number of neighbor of the processus;
% \item $myindexVi(:)$ : list of each neighbors of the domain by rank processus;
% \item $myptrindexVi(:)$ : pointer to the first node of the interface of each domain;
% \item $myindexintrf(:)$ : list of the node of the domain interface, index are in local ordering;
% \item $myinterface(:)$ : conversion from local ordering to global ordering;
% \item $myndoflogicintrf$ : Integer corresponding to the number of node in the interface wich who the domain is responsable for;
% \item $mylogicintrf(:)$ : list of the nodes that this domain is responsible for (in the local ordering).
% \end{itemize}
% 
% \subsubsection{Example with finite element interface}
% 
% According to this input graph of the matrix already distributed amound processors  as describe in figure \ref{matrixgraph}.
% 
% \begin{figure}
%   \centering
%   \includegraphics[height=0.6\linewidth]{figures/finiteelementmatrix.pdf}
%   \caption{The separators are circled in red, the number in bold are the processus id, the nodes number are in global ordering }
%   \label{matrixgraph}
% \end{figure}
% 
% The users will have to call the function with the parameters of xmph\_create\_domains defined as follow for each processus :
% 
% 
% \center
% \begin{tabular}{|c|c|}
% \hline
% \begin{minipage}{0.5\linewidth}
% 
% \vspace{0.8em}
% domain $0$ :
% \\
% 
% $myndof$ :
% \begin{tabular}{|l|}
% \hline
% 7 \\
% \hline
% \end{tabular}
% 
% $myndofinterior$ :
% \begin{tabular}{|l|}
% \hline
% 2 \\
% \hline
% \end{tabular}
% 
%         
% $myndofintrf$ :
% \begin{tabular}{|l|}
% \hline
% 5 \\
% \hline
% \end{tabular}
% 
% 
% $myint\_Lg$ :
% \begin{tabular}{|l|}
% \hline
% 0 \\
% \hline
% \end{tabular}
% 
% $lenindintrf$ :
% \begin{tabular}{|l|}
% \hline
% 10 \\
% \hline
% \end{tabular}
% 
% $mysizeintrf$ :
% \begin{tabular}{|l|}
% \hline
% 5 \\
% \hline
% \end{tabular}
% 
% $gballintrf$ :
% \begin{tabular}{|l|}
% \hline
% 6 \\
% \hline
% \end{tabular}
% 
% $mynbvi$ :
% \begin{tabular}{|l|}
% \hline
% 3 \\
% \hline
% \end{tabular}
% 
% $myindexVi(:)$ :
% \begin{tabular}{|l|l|l|}
% \hline
% 3 & 2 & 1 \\
% \hline
% \end{tabular}
% 
% $myptrindexVi(:)$ :
% \begin{tabular}{|l|l|l|l|}
% \hline
% 1 & 3 & 6 & 11 \\
% \hline
% \end{tabular}
% 
% $myindexintrf(:)$ :
% \begin{tabular}{|*{10}{c|}}
% \hline
% 4 & 5 & 3 & 4 & 5 & 3 & 4 & 5 & 1 & 2 \\
% \hline
% \end{tabular}
% 
% $myinterface(:)$ :
% \begin{tabular}{|l|l|l|l|l|}
% \hline
% 4 & 5 & 1 & 2 & 3 \\
% \hline
% \end{tabular}
% 
% $myndoflogicintrf$ :
% \begin{tabular}{|l|}
% \hline
% 5 \\
% \hline
% \end{tabular}
% 
% 
% $mylogicintrf(:)$ :
% \begin{tabular}{|l|l|l|l|l|}
% \hline
% 3 & 4 & 5 & 6 & 7 \\
% \hline
% \end{tabular}
% \\
% \end{minipage}
% &
% 
% \begin{minipage}{0.5\linewidth}
% \vspace{0.8em}
%  domain $1$ :
% \\
% 
% $myndof$ :
% \begin{tabular}{|l|}
% \hline
% 6 \\
% \hline
% \end{tabular}
% 
% $myndofinterior$ :
% \begin{tabular}{|l|}
% \hline
% 1 \\
% \hline
% \end{tabular}
% 
%         
% $myndofintrf$ :
% \begin{tabular}{|l|}
% \hline
% 5 \\
% \hline
% \end{tabular}
% 
% 
% $myint\_Lg$ :
% \begin{tabular}{|l|}
% \hline
% 0 \\
% \hline
% \end{tabular}
% 
% $lenindintrf$ :
% \begin{tabular}{|l|}
% \hline
% 10 \\
% \hline
% \end{tabular}
% 
% $mysizeintrf$ :
% \begin{tabular}{|l|}
% \hline
% 5 \\
% \hline
% \end{tabular}
% 
% $gballintrf$ :
% \begin{tabular}{|l|}
% \hline
% 6 \\
% \hline
% \end{tabular}
% 
% $mynbvi$ :
% \begin{tabular}{|l|}
% \hline
% 3 \\
% \hline
% \end{tabular}
% 
% $myindexVi(:)$ \begin{tabular}{|l|l|l|}
% \hline
% 3 & 2 & 0 \\
% \hline
% \end{tabular}
% 
% $myptrindexVi(:)$ :
% \begin{tabular}{|l|l|l|l|}
% \hline
% 1 & 3 & 6 & 11 \\
% \hline
% \end{tabular}
% 
% $myindexintrf(:)$ :
% \begin{tabular}{|*{10}{c|}}
% \hline
% 4 & 5 & 3 & 4 & 5 & 3 & 4 & 5 & 1 & 2 \\
% \hline
% \end{tabular}
% 
% 
% $myinterface(:)$ :
% \begin{tabular}{|l|l|l|l|l|}
% \hline
% 4 & 5 & 1 & 2 & 3 \\
% \hline
% \end{tabular}
% 
% $myndoflogicintrf$ :
% \begin{tabular}{|l|}
% \hline
% 0 \\
% \hline
% \end{tabular}
% 
% 
% $mylogicintrf(:)$ :
% \begin{tabular}{|l|}
% \hline
%  \\
% \hline
% \end{tabular}
% \\
% \end{minipage}
% \\
% \hline
% \begin{minipage}{0.5\linewidth}
% \vspace{0.8em}
%  domain $2$ :
% \\
% 
% $myndof$ :
% \begin{tabular}{|l|}
% \hline
% 6 \\
% \hline
% \end{tabular}
% 
% $myndofinterior$ :
% \begin{tabular}{|l|}
% \hline
% 2 \\
% \hline
% \end{tabular}
% 
%         
% $myndofintrf$ :
% \begin{tabular}{|l|}
% \hline
% 4 \\
% \hline
% \end{tabular}
% 
% 
% $myint\_Lg$ :
% \begin{tabular}{|l|}
% \hline
% 0 \\
% \hline
% \end{tabular}
% 
% $lenindintrf$ :
% \begin{tabular}{|l|}
% \hline
% 9 \\
% \hline
% \end{tabular}
% 
% $mysizeintrf$ :
% \begin{tabular}{|l|}
% \hline
% 4 \\
% \hline
% \end{tabular}
% 
% $gballintrf$ :
% \begin{tabular}{|l|}
% \hline
% 6 \\
% \hline
% \end{tabular}
% 
% $mynbvi$ :
% \begin{tabular}{|l|}
% \hline
% 3 \\
% \hline
% \end{tabular}
% 
% $myindexVi(:)$ :
% \begin{tabular}{|l|l|l|}
% \hline
% 3 & 1 & 0 \\
% \hline
% \end{tabular}
% 
% $myptrindexVi(:)$ :
% \begin{tabular}{|l|l|l|l|}
% \hline
% 1 & 4 & 7 & 10 \\
% \hline
% \end{tabular}
% 
% $myindexintrf(:)$ :
% \begin{tabular}{|*{9}{c|}}
% \hline
% 3 & 4 & 1 & 2 & 3 & 4 & 2 & 3 & 4 \\
% \hline
% \end{tabular}
% 
% $myinterface(:)$ :
% \begin{tabular}{|l|l|l|l|}
% \hline
% 6 & 1 & 2 & 3 \\
% \hline
% \end{tabular}
% 
% $myndoflogicintrf$ :
% \begin{tabular}{|l|}
% \hline
% 1 \\
% \hline
% \end{tabular}
% 
% 
% $mylogicintrf(:)$ :
% \begin{tabular}{|l|}
% \hline
% 3 \\
% \hline
% \end{tabular}
% \\
% \end{minipage}
% 
% &
% 
% \begin{minipage}{0.5\linewidth}
% \vspace{0.8em}
% domain $3$ :
% \\
% 
% $myndof$ :
% \begin{tabular}{|l|}
% \hline
% 4 \\
% \hline
% \end{tabular}
% 
% $myndofinterior$ :
% \begin{tabular}{|l|}
% \hline
% 1 \\
% \hline
% \end{tabular}
% 
%         
% $myndofintrf$ :
% \begin{tabular}{|l|}
% \hline
% 3 \\
% \hline
% \end{tabular}
% 
% 
% $myint\_Lg$ :
% \begin{tabular}{|l|}
% \hline
% 0 \\
% \hline
% \end{tabular}
% 
% $lenindintrf$ :
% \begin{tabular}{|l|}
% \hline
% 7 \\
% \hline
% \end{tabular}
% 
% $mysizeintrf$ :
% \begin{tabular}{|l|}
% \hline
% 3 \\
% \hline
% \end{tabular}
% 
% $gballintrf$ :
% \begin{tabular}{|l|}
% \hline
% 6 \\
% \hline
% \end{tabular}
% 
% $mynbvi$ :
% \begin{tabular}{|l|}
% \hline
% 3 \\
% \hline
% \end{tabular}
% 
% $myindexVi(:)$ :
% \begin{tabular}{|l|l|l|}
% \hline
% 2 & 1 & 0 \\
% \hline
% \end{tabular}
% 
% $myptrindexVi(:)$ :
% \begin{tabular}{|l|l|l|l|}
% \hline
% 1 & 4 & 6 & 8 \\
% \hline
% \end{tabular}
% 
% $myindexintrf(:)$ :
% \begin{tabular}{|l|l|l|l|l|l|l|}
% \hline
% 2 & 3 & 1 & 2 & 3 & 2 & 3 \\
% \hline
% \end{tabular}
% 
% $myinterface(:)$ :
% \begin{tabular}{|l|l|l|l|}
% \hline
% 6 & 2 & 3 \\
% \hline
% \end{tabular}
% 
% $myndoflogicintrf$ :
% \begin{tabular}{|l|}
% \hline
% 0 \\
% \hline
% \end{tabular}
% 
% 
% $mylogicintrf(:)$ :
% \begin{tabular}{|l|l|l|l|}
% \hline
% \\
% \hline
% \end{tabular}
% \\
% \end{minipage}
% 
% \\
% \hline
% \end{tabular}
% 
% The local matrices according to this decomposition is described in figure \ref{locmatrix}.
% 
% \begin{figure}
% \centering
% \includegraphics[height=0.8\linewidth]{figures/locmatrix}
% \caption{The different matrices with their local and global orderings. The interface nodes are in red, the interior nodes are in black}
% \label{locmatrix}
% \end{figure}
% 
% The user has to describe this matrix and the right hand side as in the centralized interface, except that the matrix and rhs given will be the local ones and not the global one.
%

%%%
% Hidden feature

\iftoggle{devversion}{
  { \color{red}
  \subsubsection{Use a custom matrix to assemble the Schur}
  \emph{note : Hidden feature}

  When using the distributed interface, it is possible for the user to give an extra sparse matrix ${\cal T}_i$ in each
  subdomain to assemble the Schur. Instead of computing the restriction of the Schur complement to the interface
  $\Gamma_i$ based on the contribution of its neighbors (equations \ref{eq:MAS} and \ref{eq:Schur1D}), the
  assembled local Schur will be computed by adding ${\cal T}_i$ to the local Schur:

  \begin{equation}
    \bar{\cal  S}_i  =  S_i + {\cal T}_i
  \end{equation}

  This feature can be used by setting ICNTL(41) to $1$ and defining the matrix ${\cal T}_i$ as follows:

  \begin{itemize}
  \item \textbf{mphs\%BC\_N  } (integer) specifies the order of the ${\cal T}_i$ (must be the size of the Schur)
  \item \textbf{mphs\%BC\_NNZ} (integer) specifies the number of the entries in ${\cal T}_i$ (hence $NNZ > 0$ )
  \item \textbf{mphs\%BC\_ROWS} (integer array of size NNZ) lists the row indices of ${\cal T}_i$ entries
  \item \textbf{mphs\%BC\_COLS} (integer array of size NNZ) lists the column indices of ${\cal T}_i$ entries
  \item \textbf{mphs\%BC\_VALUES} (Real/Complex array of size NNZ) lists the values  of ${\cal T}_i$ entries
  \end{itemize}

  The symmetry of ${\cal T}_i$ is assumed to be the same as the input matrix $A_i$ (controled by mphs\%SYM).
  \\
  \textit{Warning:} the  ordering of  the interface  nodes
  into \texttt{myinterface} has  to  match the  ordering of the nodes in ${\cal T}_i$.
  \\
  Here are the nodes to put in the matrix for the graph given Figure~\ref{fig:dd}.
  
  \begin{center}
    \begin{tabular}{llll}
      Domain & bc\%n & Node order (local index) & Node order (global index) \\
      \hline
      1 & 6 & [7,8,9,10,11,12] & [13,14,15,16,10,4] \\
      2 & 5 & [5,6,7,8,9]      & [18,17,16,10,4]    \\
      3 & 6 & [7,8,9,10,11,12] & [13,14,15,16,22,28] \\
      4 & 5 & [5,6,7,8,9]      & [18,17,16,22,28] \\
      &  & \\
    \end{tabular}
  \end{center}
  
  }
}

\subsection{Writing a matrix to a file}

The user may get the preprocessed input matrix (assembled and
eventually symmetrised in structure) by setting the string
mphs\%\textbf{WRITE\_MATRIX} (string) on process 0 before the analysis
step (mphs\%JOB=1). This matrix will be written during the analysis
step on the file mphs\%\textbf{WRITE\_MATRIX} in matrix market format.


\subsection{Using two levels of parallelism inside \maphys{}}
\label{sec:two-level-parallelism}

The current release of \maphys{} supports two levels of parallelism
(MPI+threads). The MPI level is inherent to \maphys{} and cannot be
disabled. By default, only the MPI level of parallelism is enabled and
the user can optionnally furthermore activate multithreading using the
following control parameters:

\begin{itemize}
\item \ref{itm:icntl42} = 1: activate the multithreading inside \maphys{};
\item \ref{itm:icntl37} = number of nodes;
\item \ref{itm:icntl38} = number of CPU cores on each nodes;
\item \ref{itm:icntl39} = number of MPI processes;
\item \ref{itm:icntl40} = number of Threads that you want to use inside each MPI process.
\end{itemize}

You can also set \ref{itm:icntl36} in order to specify the binding you want to use inside \maphys{} as follow :
\begin{itemize}
\item 0 = do not bind;
\item 1 = thread to core binding;
\item 2 = group binding.
\end{itemize}

\emph{note 1: Be carreful that if you use more threads than cores available no binding will be perform.}

\emph{note 2: Depending on the problem you want to solve it could be better to use more MPI processes than using threads you can found a study on some problems in Stojce Nakov PHD thesis.}
