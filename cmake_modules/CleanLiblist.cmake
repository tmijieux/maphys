###
#
# @copyright (c) 2009-2014 The University of Tennessee and The University
#                          of Tennessee Research Foundation.
#                          All rights reserved.
# @copyright (c) 2012-2016 Inria. All rights reserved.
# @copyright (c) 2012-2014 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
#
###
#
#  @file CMakeLists.txt
#
#  @project MORSE
#  MORSE is a software package provided by:
#     Inria Bordeaux - Sud-Ouest,
#     Univ. of Tennessee,
#     King Abdullah Univesity of Science and Technology
#     Univ. of California Berkeley,
#     Univ. of Colorado Denver.
#
#  @author Florent Pruvost
#  @date 19-08-2016
#
###

macro (clean_liblist _libin _libout)
    foreach(_lib ${${_libin}})
        get_filename_component(lib_we ${_lib} NAME)
        STRING(REPLACE "lib"    "-l" lib_we "${lib_we}")
        STRING(REPLACE ".so"    ""   lib_we "${lib_we}")
        STRING(REPLACE ".a"     ""   lib_we "${lib_we}")
        STRING(REPLACE ".dylib" ""   lib_we "${lib_we}")
        STRING(REPLACE ".dll"   ""   lib_we "${lib_we}")
        set(${_libout} "${${_libout}} ${lib_we}")
    endforeach()
endmacro()