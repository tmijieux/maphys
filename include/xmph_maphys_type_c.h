#include "mpi.h"

#ifdef __cplusplus
 extern "C" {
#endif
#include <string.h>
#include <stdio.h>

typedef struct {float r,i;} maphys_complex;
typedef struct {double r,i;} maphys_double_complex;

#define MAPHYS_STRL_C        1024
#define MAPHYS_ICNTL_SIZE_C  48
#define MAPHYS_RCNTL_SIZE_C  40 
#define MAPHYS_IINFO_SIZE_C  40
#define MAPHYS_RINFO_SIZE_C  41
#define MAPHYS_IINFOG_SIZE_C 6
#define MAPHYS_RINFOG_SIZE_C 4 
#define MAPHYS_IKEEP_SIZE_C  48
#define MAPHYS_RKEEP_SIZE_C  40   


typedef struct {

  /* ===============
     USER PARAMETERS
     ===============
        
      MPI communicator
        
      The MPI Communicator used by maphys.
      It must be set with a valid MPI Communicator before any call
      to maphys.
      */    
  MPI_Comm comm; 
  MPI_Fint fcomm;
     /* Input matrix (in coordinate format)
	--------------------------------------- */
     
     /* matrix symmetry
      - 0 = unsymmetric
      - 1 = symmetric
      - 2 = SPD
      . */
  int sym; 
  /* !> order of the matrix */
  int n;
     /*  number of entries in the sparse matrix.
      If the matrix is symmetric or SPD,
      user only gives half of the entries. */
  int  nnz;
  int  *rows;
  int  *cols;
  XMPH_CFLOAT  *values;

  // If the user want to give the permutation he had to give some other info 
  int  *permtab;
  int  *sizetab;

  // ask to write the input matrix to a file
  //string write_matrix;

  // Right-hand-side (in dense format ordered by columns)
  // ----------------------------------------------------
  int nrhs;  
  XMPH_CFLOAT *rhs; 

  // Solution (in dense format ordered by columns)
  // ----------------------------------------------------
  XMPH_CFLOAT *sol;

  // Controls
  // --------
  int job ; 
  int   icntl[MAPHYS_ICNTL_SIZE_C];
  double rcntl[MAPHYS_RCNTL_SIZE_C];
     
  // Statistics
  // -----------
  // version
  char version[MAPHYS_STRL_C+1+1];

  // on this process (MPI)

  int  iinfo[MAPHYS_IINFO_SIZE_C]; 
  double rinfo[MAPHYS_RINFO_SIZE_C]; 
     
  // on all process (MPI)

  int iinfomin[MAPHYS_IINFO_SIZE_C];
  int iinfomax[MAPHYS_IINFO_SIZE_C]; 
  int iinfoavg[MAPHYS_IINFO_SIZE_C];
  int iinfosig[MAPHYS_IINFO_SIZE_C];

  double rinfomin[MAPHYS_RINFO_SIZE_C]; 
  double rinfomax[MAPHYS_RINFO_SIZE_C]; 
  double rinfoavg[MAPHYS_RINFO_SIZE_C];
  double rinfosig[MAPHYS_RINFO_SIZE_C]; 

  int iinfog[MAPHYS_IINFOG_SIZE_C]; 
  double rinfog[MAPHYS_RINFOG_SIZE_C]; 

  

} XMPH_maphys_t_c;
 


void xmph_maphys_driver_c( XMPH_maphys_t_c *cmaph );



void xmph_maphys_create_domain_c(XMPH_maphys_t_c * maphys_par, int myndof, int myndofinterior, int myndofintrf, 
			       int lenindintrf, int mysizeintrf, int gballintrf, int mynbvi, int* myindexvi,
			       int * myptrindexvi, int* myindexintrf, int* myinterface, int myndoflogicintrf, int *mylogicintrf,
			       double* weight);

void XMPH_distributed_interface_c(XMPH_maphys_t_c * maphys_par, int myndof,
				  int mysizeintrf, int* myinterface, int mynbvi, int* myindexvi,
				  int * myptrindexvi, int* myindexintrf);
#ifdef __cplusplus
 }
#endif
