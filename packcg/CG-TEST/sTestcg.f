*************************************************************************
**                 TEST PROGRAMME FOR THE CG CODE
*************************************************************************

      program validation
*
      integer lda,  lwork
      parameter (lda = 1000)
      parameter (lwork = 8*lda+2)
*
      integer i, j, n
      integer revcom, colx, coly, colz
      integer irc(7), icntl(7), info(3)
*
      integer matvec, precondLeft,  dotProd
      parameter (matvec=1, precondLeft=2, dotProd=3)
*
      integer nout
*
      real  a(lda,lda), work(lwork)
      real  cntl(5), rinfo(2)
*
      real ZERO, ONE, aux
      parameter (ZERO = 0.0e0, ONE = 1.0e0)
*
      real sdot
      external sdot
*
***************************************************************
** Generate the test matrix a and set the right-hand side
** in positions (n+1) to 2n of the array work.
** The right-hand side is chosen such that the exact solution
** is the vector of all ones.
***************************************************************
*
      write(*,*) '***********************************************'
      write(*,*) 'This code is an example of use of CG'
      write(*,*) 'in single precision real arithmetic'
      write(*,*) 'Results are written in output files'
      write(*,*) 'fort.10 : log file of CG iterations '
      write(*,*) 'and sol_Testcg : output of the computation.'
      write(*,*) '***********************************************'
      write(*,*)
      write(*,*) 'Matrix size < ', lda
      read(*,*) n
      if (n.gt.lda) then
        write(*,*) 'You are asking for a too large matrix'
        goto 100
      endif
*
      do j = 1,n
        do i = 1,n
          a(i,j) = ZERO
        enddo
        aux = mod(j,7)
        work(j) = sqrt(aux)
      enddo
*
      do i = 1,n
        a(i,i) = 8.0e0
      enddo
      do i = 1,n-1
        a(i,i+1) = -2.0e0
        a(i+1,i) = -2.0e0
      enddo
*
      call SGEMV('N',n,n,ONE,A,lda,work(1),1,ZERO,work(n+1),1)
*
      do j = 1,n
         work(j) = ZERO
      enddo
*
*******************************************************
** Initialize the control parameters to default value
*******************************************************
* setup the monitoring CG variables to default values
        call init_scg(icntl,cntl)
*
        icntl(5) = 0
*
* Define the bound for the stopping criterion
        cntl(1)  = 1.0 d  -6
* Define the stream for the convergence history
        icntl(3) = 10
* Define the maximum number of iterations
        icntl(6) = n
* Ask for the estimation of the condition number (if icntl(1) == 1)
        icntl(7) = 1
* No preconditioner is provided
        icntl(4) = 0
*
*****************************************
** Reverse communication implementation
*****************************************
*
10     call drive_scg(n,n,lwork,work,
     &         irc,icntl,cntl,info,rinfo)

       revcom = irc(1)
       colx   = irc(2)
       coly   = irc(3)
       colz   = irc(4)
*
       if (revcom.eq.matvec) then
* perform the matrix vector product for the CG iteration
*        work(colz) <-- A * work(colx)
         call ssymv ('U', n, ONE, A, lda,
     &            work(colx), 1, ZERO , work(colz), 1)
           goto 10
*
        else if (revcom.eq.precondleft) then
*        work(colz) <-- M * work(colx)
           call scopy(n,work(colx),1,work(colz),1)
           goto 10
        else if (revcom.eq.dotProd) then
*      perform the scalar product for the CG iteration
*      work(colz) <-- work(colx) work(coly)
*
         work(colz)= sdot(n, work(colx),1, work(coly),1)
         goto 10
       endif
*
*******************************
* dump the solution on a file
*******************************
*
      if (icntl(7).eq.1) then
         write(*,*) ' Est. smallest eig. ', rinfo(2)
         write(*,*) ' Est. biggest  eig. ', rinfo(3)
      endif
*
      nout = 11
      open(nout,FILE='sol_sTestcg',STATUS='unknown')
      write(nout,*) 'info(1) = ',info(1),'  info(2) = ',info(2)
      write(nout,*) 'rinfo(1) = ',rinfo(1)
      write(nout,*) 'rinfo(2) = ',rinfo(2),'  rinfo(3) = ',rinfo(3)
      write(nout,*) 'Optimal workspace = ', info(3)
      write(nout,*) 'Solution : '
      do j=1,n
        write(nout,*) work(j)
        if (j.le.min(n,10)) write(*,*) j,work(j)
      enddo
      write(nout,*) '   '
*
100    continue
*
      stop
      end
