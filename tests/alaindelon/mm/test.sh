#!/bin/bash
export EXEC=$1

mpirun -np 3 $EXEC ./param.in | grep "sol(" > res.txt

#Check if output file differs from expected solution

RES=`diff res.txt sol.txt | wc -l`
echo $RES
if [ ${RES} -gt 0 ]
then
    echo 'Failed:'
    echo ''
    echo 'Expected:'
    cat sol.txt
    echo ''
    echo 'Found:'
    cat res.txt
else
    echo 'Passed'
fi
rm res.txt
