#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"
#include "dmph_maphys_type_c.h"
#define JOB_INIT -1
#define JOB_END -2
#define USE_COMM_WORLD -987654

// Test of multi-RHS in distributed mode
//
// [[ 4. -1.  0. -1.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.]    [[  1.  16.  31.  46.]    [[  -2.   28.   58.   88.]
//  [-1.  4. -1.  0. -1.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.]     [  2.  17.  32.  47.]     [  -1.   14.   29.   44.]
//  [ 0. -1.  4.  0.  0. -1.  0.  0.  0.  0.  0.  0.  0.  0.  0.]     [  3.  18.  33.  48.]     [   4.   34.   64.   94.]
//  [-1.  0.  0.  4. -1.  0. -1.  0.  0.  0.  0.  0.  0.  0.  0.]     [  4.  19.  34.  49.]     [   3.   18.   33.   48.]
//  [ 0. -1.  0. -1.  4. -1.  0. -1.  0.  0.  0.  0.  0.  0.  0.]     [  5.  20.  35.  50.]     [   0.    0.    0.    0.]
//  [ 0.  0. -1.  0. -1.  4.  0.  0. -1.  0.  0.  0.  0.  0.  0.]     [  6.  21.  36.  51.]     [   7.   22.   37.   52.]
//  [ 0.  0.  0. -1.  0.  0.  4. -1.  0. -1.  0.  0.  0.  0.  0.]     [  7.  22.  37.  52.]     [   6.   21.   36.   51.]
//  [ 0.  0.  0.  0. -1.  0. -1.  4. -1.  0. -1.  0.  0.  0.  0.]  x  [  8.  23.  38.  53.]  =  [   0.    0.    0.    0.]
//  [ 0.  0.  0.  0.  0. -1.  0. -1.  4.  0.  0. -1.  0.  0.  0.]     [  9.  24.  39.  54.]     [  10.   25.   40.   55.]
//  [ 0.  0.  0.  0.  0.  0. -1.  0.  0.  4. -1.  0. -1.  0.  0.]     [ 10.  25.  40.  55.]     [   9.   24.   39.   54.]
//  [ 0.  0.  0.  0.  0.  0.  0. -1.  0. -1.  4. -1.  0. -1.  0.]     [ 11.  26.  41.  56.]     [   0.    0.    0.    0.]
//  [ 0.  0.  0.  0.  0.  0.  0.  0. -1.  0. -1.  4.  0.  0. -1.]     [ 12.  27.  42.  57.]     [  13.   28.   43.   58.]
//  [ 0.  0.  0.  0.  0.  0.  0.  0.  0. -1.  0.  0.  4. -1.  0.]     [ 13.  28.  43.  58.]     [  28.   58.   88.  118.]
//  [ 0.  0.  0.  0.  0.  0.  0.  0.  0.  0. -1.  0. -1.  4. -1.]     [ 14.  29.  44.  59.]     [  17.   32.   47.   62.]
//  [ 0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0. -1.  0. -1.  4.]]    [ 15.  30.  45.  60.]]    [  34.   64.   94.  124.]]

int main(int argc, char** argv){

  DMPH_maphys_t_c id;
  int rank;
  int job;
  int i, j, i_glob;
  double tol;
  int success, my_success;

  int n = 9;
  int nrhs = 2;
  int nnz = 33;
  int n_glob = 15;

  double * a, * sol, * rhs, * th_sol;
  int * irn, * jcn;

  int myndof=n, mysizeIntrf=3, mynbvi=1;
  int * myinterface;
  int * myindexvi;
  int * myptrindexvi;
  int * myindexintrf;

  int reorder[n_glob];

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  id.job = JOB_INIT;
  id.sym = 0;
  id.comm = MPI_COMM_WORLD;
  dmph_maphys_driver_c(&id);

  a = (double *) malloc(nnz * sizeof(double));
  sol = (double *) malloc(n*nrhs * sizeof(double));
  th_sol = (double *) malloc(n*nrhs * sizeof(double));
  rhs = (double *) malloc(n*nrhs * sizeof(double));

  irn = (int *) malloc(nnz * sizeof(int));
  jcn = (int *) malloc(nnz * sizeof(int));

  i = 0;
  irn[i] = 1 ; jcn[i] = 1 ; a[i] = 4.0  ; i = i + 1;
  irn[i] = 1 ; jcn[i] = 2 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 1 ; jcn[i] = 4 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 2 ; jcn[i] = 1 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 2 ; jcn[i] = 2 ; a[i] = 4.0  ; i = i + 1;
  irn[i] = 2 ; jcn[i] = 3 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 2 ; jcn[i] = 5 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 3 ; jcn[i] = 2 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 3 ; jcn[i] = 3 ; a[i] = 4.0  ; i = i + 1;
  irn[i] = 3 ; jcn[i] = 6 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 4 ; jcn[i] = 1 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 4 ; jcn[i] = 4 ; a[i] = 4.0  ; i = i + 1;
  irn[i] = 4 ; jcn[i] = 5 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 4 ; jcn[i] = 7 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 5 ; jcn[i] = 2 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 5 ; jcn[i] = 4 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 5 ; jcn[i] = 5 ; a[i] = 4.0  ; i = i + 1;
  irn[i] = 5 ; jcn[i] = 6 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 5 ; jcn[i] = 8 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 6 ; jcn[i] = 3 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 6 ; jcn[i] = 5 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 6 ; jcn[i] = 6 ; a[i] = 4.0  ; i = i + 1;
  irn[i] = 6 ; jcn[i] = 9 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 7 ; jcn[i] = 4 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 7 ; jcn[i] = 7 ; a[i] = 2.0  ; i = i + 1;
  irn[i] = 7 ; jcn[i] = 8 ; a[i] = -0.5 ; i = i + 1;
  irn[i] = 8 ; jcn[i] = 5 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 8 ; jcn[i] = 7 ; a[i] = -0.5 ; i = i + 1;
  irn[i] = 8 ; jcn[i] = 8 ; a[i] = 2.0  ; i = i + 1;
  irn[i] = 8 ; jcn[i] = 9 ; a[i] = -0.5 ; i = i + 1;
  irn[i] = 9 ; jcn[i] = 6 ; a[i] = -1.0 ; i = i + 1;
  irn[i] = 9 ; jcn[i] = 8 ; a[i] = -0.5 ; i = i + 1;
  irn[i] = 9 ; jcn[i] = 9 ; a[i] = 2.0  ; i = i + 1;

  id.n = n;
  id.nnz = nnz;
  id.nrhs = nrhs;
  id.rows = irn;
  id.cols = jcn;
  id.sol = sol;
  id.values = a;
  id.rhs = rhs;

  if(rank == 0){
    rhs[0] = -2.0 ; rhs[1] = -1.0 ; rhs[2] = 4.0 ; rhs[3] = 3.0 ; rhs[4] = 0.0 ;
    rhs[5] = 7.0 ; rhs[6] = 6.0 ; rhs[7] = 0.0 ; rhs[8] = 10.0 ; rhs[9] = 28.0 ;
    rhs[10] = 14.0 ; rhs[11] = 34.0 ; rhs[12] = 18.0 ; rhs[13] = 0.0 ; rhs[14] = 22.0 ;
    rhs[15] = 21.0 ; rhs[16] = 0.0 ; rhs[17] = 25.0 ;

    for(i=0; i<n; i++){
      th_sol[i] = i+1;
      th_sol[i+n] = n_glob + i + 1;
    }

  }
  else{
    rhs[0] = 28.0 ; rhs[1] = 17.0 ; rhs[2] = 34.0 ; rhs[3] = 9.0 ; rhs[4] = 0.0 ;
    rhs[5] = 13.0 ; rhs[6] = 0.0 ; rhs[7] = 0.0 ; rhs[8] = 0.0 ; rhs[9] = 58.0 ;
    rhs[10] = 32.0 ; rhs[11] = 64.0 ; rhs[12] = 24.0 ; rhs[13] = 0.0 ; rhs[14] = 28.0 ;
    rhs[15] = 0.0 ; rhs[16] = 0.0 ; rhs[17] = 0.0 ;

    for(i=0;i<6;i++){
      reorder[i] = -1;
    }
    reorder[6] = 6 ; reorder[7] = 7 ; reorder[8] = 8 ;
    reorder[9] = 3 ; reorder[10] = 4 ; reorder[11] = 5 ;
    reorder[12] = 0 ; reorder[13] = 1 ; reorder[14] = 2 ;

    for(i_glob=0; i_glob < n_glob; i_glob++){
      i = reorder[i_glob];
      if(i >= 0){
        th_sol[i] = i_glob + 1;
        th_sol[i+n] = i_glob + n_glob + 1;
      }
    }
  }

#define ICNTL(I) icntl[(I)-1]
#define RCNTL(I) rcntl[(I)-1]

  // Set MaPHyS options
  tol = 1.0e-8 ;
  id.ICNTL(6) = 1;
  id.ICNTL(20) = 4; // IBBGMRESDR
  id.ICNTL(29) = 0; // 0: nothing, 1: IB; 2: QR
  id.ICNTL(21) = 1; // Precond
  id.ICNTL(24) = 500; // Niter max
  id.ICNTL(26) = 250; // Restart
  id.ICNTL(22) = 0; // MGS
  id.ICNTL(34) = 0; // Convergence history
  id.RCNTL(21) = tol; // Single tolerance
  id.RCNTL(11) = 1.0e-6;
  id.ICNTL(43) = 2;
  id.ICNTL(28) = 0;

  // Distribution
  myinterface = (int *) malloc(3 * sizeof(int));
  myindexvi = (int *) malloc(1 * sizeof(int));
  myptrindexvi = (int *) malloc(2 * sizeof(int));
  myindexintrf = (int *) malloc(3 * sizeof(int));

  myinterface[0] = 7; myinterface[1] = 8; myinterface[2] = 9;
  myindexvi[0] = 1 - rank;
  myptrindexvi[0] = 1; myptrindexvi[1] = 4;
  myindexintrf[0] = 1; myindexintrf[1] = 2; myindexintrf[2] = 3;

  DMPH_distributed_interface_c(&id, myndof, mysizeIntrf, myinterface, mynbvi, myindexvi, myptrindexvi, myindexintrf);

  id.job = 6;
  dmph_maphys_driver_c(&id);

  MPI_Barrier(MPI_COMM_WORLD);

  if(rank == 0){
    for(i = 0; i < n; ++i){
      printf("%d\t", i+1);
      for(j = 0; j < nrhs; ++j){
        printf("%f\t", id.sol[j*n+i]);
      }
      printf("\n");
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);

  if(rank == 1){
    for(i_glob = n+1; i_glob < n_glob; ++i_glob){
      i = reorder[i_glob];
      printf("%d\t", i_glob+1);
      for(j = 0; j < nrhs; ++j){
        printf("%f\t", id.sol[j*n+i]);
      }
      printf("\n");
    }
  }

  my_success = 1;
  for(i = 0; i < n*nrhs; i++){
    if(fabs(id.sol[i] - th_sol[i]) > tol){
      my_success = 0;
      printf("ERROR %d\n", rank);
    }
  }

  MPI_Reduce(&my_success, &success, 1, MPI_INT, MPI_LAND, 0, MPI_COMM_WORLD);

  if(success && rank == 0) printf("SUCCESS\n");

  id.job = JOB_END;
  dmph_maphys_driver_c(&id);

  free(rhs);
  free(sol);
  free(th_sol);
  free(a);
  free(irn);
  free(jcn);

  MPI_Finalize();

  return EXIT_SUCCESS;
}
